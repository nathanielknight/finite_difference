#!/usr/bin/python
'''
Given 
 * an unperturbed gradient file
 * an unperturbed cost-function file
 * perturbed cost-function files

Compare the gradient obtained by the adjoint method to 
the gradients estimated by performing a finite difference
of cost functions.

Requires manual unpacking of gradients from gctm.gdt.x
'''
import os, re, itertools
import numpy as np
from scipy.stats import linregress
import matplotlib.pyplot as plt

from helpers.ij_tools import ij_to_dict, map_ij

#Metadata & Helpers
plt_dir = "./plt"
species = ["ocpi", "ocpo", "bcpi", "bcpo"]
sources = ["an", "bf"]
tracers = ["{}_{}".format(spcs, src)
           for spcs, src in itertools.product(species, sources)]

def read_sparse_ij(filename, filler=0.0):
    f = file(filename, 'r')
    ijs = [map(float, a.split()) for a in f.readlines()]
    ijs = {(a,b):c for a,b,c in ijs}
    ij = [[i, j,ijs.get((i,j), filler)] for i,j in 
          itertools.product(range(1,73), range(1,47))]
    return np.array(ij)



#Load unperturbed cost function, IJ's of interest (only once)
cf_u = read_sparse_ij("./fd_data/cf_u", 0.0)
raw_cf_u = [map(float, a.split()) for a in file("./fd_data/cf_u").readlines()]
ijs = [(i,j) for i,j,k in raw_cf_u]



#Analysis
#--cf_gradient (adj_gradient gets loaded directly)
def fd(a,b, dx=0.05):
    return (a-b) / dx

def cf_grad(cf_u, cf_p):
    grad = map_ij(fd, cf_p, cf_u)
    return {(i,j):k for i,j,k in grad}


#--comparison
def compare(cf_grad, adj_grad):
    '''Returns the (fd_grad, adj_grad) for *only the keys in cf_grad*.'''
    adj_grad = {ij:adj_grad[ij] for ij in cf_grad.keys()}
    return [cf_grad[ij] for ij in cf_grad.keys()], \
        [adj_grad[ij] for ij in cf_grad.keys()]


#--plotting
def rangeof(xs):
    a = min(xs)
    b = max(xs)
    return np.linspace(a,b,1000)


def plot_fd(xs, ys, filename=None, title=None, **kwargs):
    '''Given finite-diff and adjoint gradients of the form {coord:val}
    plot them along with a best-fit line and an x=y line.'''
    assert all([filename, title]), "Must provide filename and title."
    plt.xlabel("Finite Difference Gradient")
    plt.ylabel("Adjoint Gradient")
    plt.title(title)

    #Linear Fit
    slope, intercept, rvalue, pvalue, stderr = linregress(xs, ys)
    fitf =  lambda t: slope * t + intercept

    #Draw Data
    ts = rangeof(xs)
    plt.plot(xs,ys, "ko", label="(F.D., Adj.)")
    plt.plot(ts, ts, "g:", label="y=x")
    plt.plot(ts, map(fitf, ts), "r--", label="Fit (R={:4.3})".format(rvalue))

    #Save and Finish
    plt.legend(loc=2)
    plt.savefig(os.path.join(plt_dir, filename))
    plt.clf()
    

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    for tracer in tracers:
        try:
            cf_p = read_sparse_ij("fd_data/cf_p_{}".format(tracer))
        except IOError:
            print "WARNING! missing fd_data for {}".format(tracer)
            continue

        cfg = cf_grad(cf_u, cf_p)
        adg = ij_to_dict(read_sparse_ij("fd_data/{}.gdt".format(tracer)))

        plot_fd(*compare(cfg, adg),
                filename="{}.png".format(tracer),
                title=tracer.upper())
                         
                         
        
