#Finite Difference Test for GEOS-Chem Adjoint

Perform a "finite difference test" by:

 * turning off transport in the model
 * doing a forward run and recording the relevant gradient and cost-function-by-cell (cf_u)
 * perturbing the default scale factor of a tracer you're optimizing
 * doing the adjoint again, and recording the cost function (cf_p)
 * comparing the adjoint gradent from the unperturbed run to (cf_p - cf_u) / perturbation

I find that +/- 0.05 is a good perturbation for my cost function.
