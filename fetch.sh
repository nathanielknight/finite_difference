#!/bin/bash

rm fd_data/*
rsync -a -e "ssh -p 23" natep@stetson.phys.dal.ca:fd_data .

pig cat fd_data/gctm.gdt.01 1 --headless | to_index | sed '/^$/d' > fd_data/bcpi_an.gdt
pig cat fd_data/gctm.gdt.01 10 --headless | to_index | sed '/^$/d' > fd_data/bcpo_bb.gdt
pig cat fd_data/gctm.gdt.01 4 --headless | to_index | sed '/^$/d' > fd_data/ocpo_an.gdt
pig cat fd_data/gctm.gdt.01 7 --headless | to_index | sed '/^$/d' > fd_data/ocpi_bf.gdt
pig cat fd_data/gctm.gdt.01 11 --headless | to_index | sed '/^$/d' > fd_data/ocpi_bb.gdt
